//
//  LoginCVC.swift
//  Learn
//
//  Created by Shoaib uddin on 20/11/2019.
//  Copyright © 2019 pixel. All rights reserved.
//

import UIKit

class LoginCVC: UICollectionViewCell {

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtEmail.text = "j.malik2694";
        txtPassword.text = "whatever";
    }
    
    @IBAction func letMeIn(_ sender: UIButton) {
        
        // Initializing Items
        let UserName: String = txtEmail.text!  as String;
        let Password: String = txtPassword.text! as String;
        
        //doing Validations
        if(!Validate.Required(text: UserName, Label: "email")){return;}
        if(!Validate.Required(text: Password, Label: "password")){return;}
        
        Losding(true);
        
        
    }
    
    @IBAction func forgetPassword(_ sender: UIButton) {
        
    }
    
    func Losding(_ flag: Bool){
        
        loader.isHidden = !flag;
        loaderView.isHidden = !flag;
        if(flag){
            loader.startAnimating();
        }else{
            loader.stopAnimating();
        }
        loginView.isHidden = flag;
        
        
    }
    
    

}
