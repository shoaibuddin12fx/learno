//
//  GalleryImageCVC.swift
//  Learn
//
//  Created by Xtreme Hardware on 17/02/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

protocol GalleryImageCVCDelegate: class {
    func setFactLike(cell: GalleryImageCVC, fact: EnFact );
    func returnThemeId(cell: GalleryImageCVC, themeid: Int );
    
}

class GalleryImageCVC: UICollectionViewCell {

    @IBOutlet weak var quoteViewCenterYConstraint: NSLayoutConstraint!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var imgLove: UIImageView!
    @IBOutlet var coverView: UIView!
    @IBOutlet var lblText: UILabel!
    @IBOutlet var lblRef: UILabel!
    @IBOutlet weak var quoteView: UIView!
    var id = -1;
    var fact: EnFact!;
    var delegate: GalleryImageCVCDelegate?
    
    override func prepareForReuse() {
        //
        let clr = StyleHelper.colorWithHexString(globalSettings.fcolor!);
        StyleHelper.setFontImageVisualsMaterial(self.imgLove, name: "favorite.border");
        self.lblText.textColor = clr
        self.lblRef.textColor = clr
        //setTextViewColor()

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let clr = StyleHelper.colorWithHexString(globalSettings.fcolor!);
        self.lblText.textColor = clr
        self.lblRef.textColor = clr	
        
        //setTextViewColor()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        quoteView.addGestureRecognizer(tap);
        coverView.addGestureRecognizer(tap);
        
        
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        print("voo");
        self.delegate?.returnThemeId(cell: self, themeid: self.id);
        
    }
    
    
    
    
    
    
    
    func setTextViewColor(){
        
        let clr = StyleHelper.colorWithHexString(globalSettings.fcolor!);
        
        // create the attributed colour
        let attributedStringColor = [NSAttributedStringKey.foregroundColor : clr];
        let attributedString = NSMutableAttributedString(string: self.lblRef.text!, attributes: attributedStringColor)
        
        self.lblRef.attributedText = attributedString
        //self.lblRef.tintColor = clr;
        
    }
    
    @IBAction func doOpenUrl(_ sender: UIButton) {
        
        if let urlString = self.lblRef.text {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                if( UIApplication.shared.canOpenURL(url as URL) ){
                    UIApplication.shared.open(url as URL, options: [:]) { (success) in
                        //
                    }
                }
            }
        }
        
        
    }
    
    
    
    func setData(_ data: EnFact){
        
        print(data);
        self.fact = data;
        setLikeHeart();
        self.lblText.text = data.Content;
        self.lblRef.text = data.Reference;
        if let _id = data.ID, let pid = Int(_id){
            self.id = pid;
        };
        // self.lblRef.text = "https://google.com";
        //setTextViewColor()
        
    }
    
    func setData(_ data: Background, index: Int){
        
        print(data);
        self.id = index;
        self.lblText.text = "Abcd";
        
        
        if(data.ttype == "B"){
            
            if(data.background != nil){
                FileApi.retrieveImageFromDocFolder(name: data.background!) { (image) in
                    self.imageView.image = image;
                }
            }
            
            self.imageView.isHidden = false;
            
        }else{
            
            self.imageView.isHidden = true;
            
        }
        
        self.contentView.backgroundColor = StyleHelper.colorWithHexString(data.bcolor!);
        self.lblText.textColor = StyleHelper.colorWithHexString(data.fcolor!);
        self.lblText.font = UIFont(name: data.font!, size: self.lblText.font.pointSize);
        self.imgLove.isHidden = true;
        quoteViewCenterYConstraint.constant = 0;
        
        
        
        
        
        
        
    }
    
    
    
    func setFont(name: String){
        self.lblText.font = UIFont(name: name, size: lblText.font.pointSize);
    }
    
    func addTickMark(){
        let width = self.frame.width;
        let height = self.frame.height;
        
        let imageName = "check.png"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.frame = CGRect(x: width - (width * 0.2) - 10, y: height - (width * 0.2) - 10, width: width * 0.2, height: width * 0.2);
        imageView.tag = 34;
        
        
        let blurOverlay: UIView = UIView();
        blurOverlay.backgroundColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.4);
        blurOverlay.frame = CGRect(x: 0, y: 0, width: width, height: height);
        blurOverlay.tag = 35
        self.addSubview(blurOverlay);
        self.addSubview(imageView);
        
        
        
        
        
        
    }
    
    func removeTickMark(){
        
        for view in self.subviews {
            if(view.tag == 34 || view.tag == 35){
                view.removeFromSuperview();
            }
            
        }
        
    }
    
    @IBAction func doLike(_ sender: UIButton) {
        
//        var visibleRect: CGRect = CGRect()
//        visibleRect.origin = (collectionView?.contentOffset)!
//        visibleRect.size = (collectionView?.bounds.size)!
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//        let visibleIndexPath: IndexPath? = collectionView?.indexPathForItem(at: visiblePoint)
        
//        let idd = facts[(visibleIndexPath?.row)!].ID;
        
        // UtilityHelper.AlertMessage("\(idd!)");
        
        if let f = self.fact, let id = f.ID {
            self.fact.likebyme = !self.fact.likebyme;
            self.setLikeHeart();
            
            LearnottoApi.addLike(factid: id) { (success) in
                //
                if(success){
                    self.delegate?.setFactLike(cell: self, fact: f);
                }
            }
        }
        
        
    }
    
    func setLikeHeart(){
        if(self.fact.likebyme){
            StyleHelper.setFontImageVisualsMaterial(self.imgLove, name: "favorite");
        }else{
            StyleHelper.setFontImageVisualsMaterial(self.imgLove, name: "favorite.border");
        }
    }

}
